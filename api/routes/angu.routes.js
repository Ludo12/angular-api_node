module.exports = app => {
    const angus = require("../controllers/angu.controller.js");

    var router = require("express").Router();

    // Create a new Angu
    router.post("/", angus.create);

    // Retrieve all Angus
    router.get("/", angus.findAll);

    // Retrieve all published Angus
    router.get("/published", angus.findAllPublished);

    // Retrieve a single Angu with id
    router.get("/:id", angus.findOne);

    // Update a Angu with id
    router.put("/:id", angus.update);

    // Delete a Angu with id
    router.delete("/:id", angus.delete);

    // Create a new Angu
    router.delete("/", angus.deleteAll);

    app.use('/api/angus', router);
};