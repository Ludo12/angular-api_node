const db = require("../models");
const Angu = db.angu;

//créer et sauvegarder
exports.create = (req, res) => {
    // req valide
    if (!req.body.title) {
        res.status(400).send({
            message: "Le contenu ne doit pas être vide!"
        });
        return;
    }

    // Create un Angu
    const angu = new Angu({
        title: req.body.title,
        description: req.body.description,
        published: req.body.published ? req.body.published : false
    });

    // Sauvegarder Angu dans la DB
    angu
        .save(angu)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Erreur dans la création d'un Angu."
            });
        });
};

//all
exports.findAll = (req, res) => {
    const title = req.query.title;
    var condition = title ? {
        title: {
            $regex: new RegExp(title),
            $options: "i"
        }
    } : {};

    Angu.find(condition)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Erreur dans la recherche des angus."
            });
        });
};

//find one
exports.findOne = (req, res) => {
    const id = req.params.id;

    Angu.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({
                    message: "Aucun Angu avec l'id " + id
                });
            else res.send(data);
        })
        .catch(err => {
            res
                .status(500)
                .send({
                    message: "Erreur dans la recherche de l'Angu avec l'id=" + id
                });
        });
};

//update
exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "La donnée à mettre à jour ne doit pas être vide!"
        });
    }

    const id = req.params.id;

    Angu.findByIdAndUpdate(id, req.body)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update Angu with id=${id}. Maybe Angu was not found!`
                });
            } else res.send({
                message: "Angu a été mis à jour avec succes"
            });
        })
        .catch(err => {
            res.status(500).send({
                message: "Erreur dans la mise à jour de l'Andu avec l'id=" + id
            });
        });
};

//supprimer un
exports.delete = (req, res) => {
    const id = req.params.id;

    Angu.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `L'Angu avec l'id=${id} ne peut pas être supprimé. Peut être que l'Angu n'a pas été trouvé!`
                });
            } else {
                res.send({
                    message: "Angu supprimé avec succes!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Impossible de supprimer l'angu avec l'id=" + id
            });
        });
};

//supprimer tous
exports.deleteAll = (req, res) => {
    Angu.deleteMany({})
        .then(data => {
            res.send({
                message: `${data.deletedCount} Angus ont été suprimés avec succes!`
            });
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Une erreur c'est produite pour trouver les angus."
            });
        });
};

//find tous les publiés
exports.findAllPublished = (req, res) => {
    Angu.find({
            published: true
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Une erreur c'est produite pour trouver les angus."
            });
        });

};