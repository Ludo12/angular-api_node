//import des modules express et cors
const express = require("express");
const cors = require("cors");

const app = express();

let corsOptions = {
    origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

//parser
app.use(express.json());

app.use(express.urlencoded({
    extended: true
}));

//connection
const db = require("./models");
db.mongoose
    .connect(db.url, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    })
    .then(() => {
        console.log("DB connectée");
    })
    .catch(err => {
        console.log("impossible de se connecter à la DB;", err);
        process.exit();
    });

//route
app.get("/", (req, res) => {
    res.json({
        message: "Bienvenue"
    });
});

require("./routes/angu.routes")(app);

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
    console.log(`Serveur lancé sur le port ${PORT}.`);
});