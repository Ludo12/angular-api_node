import { TestBed } from '@angular/core/testing';

import { AnguService } from './angu.service';

describe('AnguService', () => {
  let service: AnguService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AnguService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
