import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Angu } from '../models/angu.model';


const baseUrl = 'http://localhost:8080/api/angus';

@Injectable({
  providedIn: 'root'
})

export class AnguService {
  constructor(private http: HttpClient) { }

  getAll(): Observable<Angu[]> {
    return this.http.get<Angu[]>(baseUrl);
  }

  get(id: any): Observable<Angu> {
    return this.http.get(`${baseUrl}/${id}`);
  }

  create(data: any): Observable<any> {
    return this.http.post(baseUrl, data);
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(baseUrl);
  }

  findByTitle(title: any): Observable<Angu[]> {
    return this.http.get<Angu[]>(`${baseUrl}?title=${title}`);
  }
}
