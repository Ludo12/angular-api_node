import { Component, OnInit } from '@angular/core';
import { Angu } from 'src/app/models/angu.model';
import { AnguService } from 'src/app/services/angu.service';

@Component({
  selector: 'app-angus-list',
  templateUrl: './angus-list.component.html',
  styleUrls: ['./angus-list.component.css']
})
export class AngusListComponent implements OnInit {
  angus?: Angu[];
  currentAngu: Angu = {};
  currentIndex = -1;
  title = '';

  constructor(private anguService: AnguService) { }

  ngOnInit(): void {
    this.retrieveAngus();
  }

  retrieveAngus(): void {
    this.anguService.getAll()
      .subscribe(
        data => {
          this.angus = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveAngus();
    this.currentAngu = {};
    this.currentIndex = -1;
  }

  setActiveAngu(angu: Angu, index: number): void {
    this.currentAngu = angu;
    this.currentIndex = index;
  }

  removeAllAngus(): void {
    this.anguService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.refreshList();
        },
        error => {
          console.log(error);
        });
  }

  searchTitle(): void {
    this.currentAngu = {};
    this.currentIndex = -1;

    this.anguService.findByTitle(this.title)
      .subscribe(
        data => {
          this.angus = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }
}
