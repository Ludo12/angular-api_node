import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AngusListComponent } from './angus-list.component';

describe('AngusListComponent', () => {
  let component: AngusListComponent;
  let fixture: ComponentFixture<AngusListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AngusListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AngusListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
