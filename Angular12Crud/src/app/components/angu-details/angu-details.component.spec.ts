import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnguDetailsComponent } from './angu-details.component';

describe('AnguDetailsComponent', () => {
  let component: AnguDetailsComponent;
  let fixture: ComponentFixture<AnguDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnguDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnguDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
