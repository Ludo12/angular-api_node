import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Angu } from 'src/app/models/angu.model';
import { AnguService } from 'src/app/services/angu.service';

@Component({
  selector: 'app-angu-details',
  templateUrl: './angu-details.component.html',
  styleUrls: ['./angu-details.component.css']
})
export class AnguDetailsComponent implements OnInit {

  currentAngu: Angu = {
    title: '',
    description: '',
    published: false
  };
  message = '';

  constructor(
    private anguService: AnguService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.message = '';
    this.getAngu(this.route.snapshot.params.id);
  }

  getAngu(id: string): void {
    this.anguService.get(id)
      .subscribe(
        data => {
          this.currentAngu = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  updatePublished(status: boolean): void {
    const data = {
      title: this.currentAngu.title,
      description: this.currentAngu.description,
      published: status
    };

    this.message = '';

    this.anguService.update(this.currentAngu.id, data)
      .subscribe(
        response => {
          this.currentAngu.published = status;
          console.log(response);
          this.message = response.message ? response.message : 'The status was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }

  updateAngu(): void {
    this.message = '';

    this.anguService.update(this.currentAngu.id, this.currentAngu)
      .subscribe(
        response => {
          console.log(response);
          this.message = response.message ? response.message : 'This angu was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }

  deleteAngu(): void {
    this.anguService.delete(this.currentAngu.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/angus']);
        },
        error => {
          console.log(error);
        });
  }

}
