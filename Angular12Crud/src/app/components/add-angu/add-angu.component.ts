import { AnguService } from './../../services/angu.service';
import { Component, OnInit } from '@angular/core';
import { Angu } from 'src/app/models/angu.model';

@Component({
  selector: 'app-add-angu',
  templateUrl: './add-angu.component.html',
  styleUrls: ['./add-angu.component.css']
})
export class AddAnguComponent implements OnInit {

  angu: Angu = {
    title: '',
    description: '',
    published: false
  };
  submitted = false;

  constructor(private anguService: AnguService) { }

  ngOnInit(): void {
  }

  saveAngu(): void {
    const data = {
      title: this.angu.title,
      description: this.angu.description
    };

    this.anguService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newAngu(): void {
    this.submitted = false;
    this.angu = {
      title: '',
      description: '',
      published: false
    };
  }

}
