import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAnguComponent } from './add-angu.component';

describe('AddAnguComponent', () => {
  let component: AddAnguComponent;
  let fixture: ComponentFixture<AddAnguComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddAnguComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAnguComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
