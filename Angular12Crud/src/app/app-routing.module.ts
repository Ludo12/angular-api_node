import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddAnguComponent } from './components/add-angu/add-angu.component';
import { AnguDetailsComponent } from './components/angu-details/angu-details.component';
import { AngusListComponent } from './components/angus-list/angus-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'angus', pathMatch: 'full' },
  { path: 'angus', component: AngusListComponent },
  { path: 'angus/:id', component: AnguDetailsComponent },
  { path: 'add', component: AddAnguComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
